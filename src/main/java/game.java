
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class game {
    private table Table;
    private Scanner kb;
    
    public void Rungame(){
        player x = new player('X');
        player o = new player('O');
        Table = new table(3,3);
        kb = new Scanner(System.in);
        boolean draw = true;
        OXPlayer player = new OXPlayer(x,o);
        
        
        System.out.println("Welcome to OX Game");
        
        Table.Maptable();
        for(int i=0; i<9; i++){
           

            System.out.println("Turn " + player.getCurrentName());
            System.out.println("Please input row, col" + ":");
            int row = kb.nextInt()-1;
            int col = kb.nextInt()-1;
            
            if(Table.check(row, col)){
                Table.setplay(row, col, player.getCurrentPlayer());
                player.switchPlayer();
            }else{
                i--;
            }
            
            Table.Maptable();
            if(i >= 4) {
                if(Table.checkwin()) {
                    player.switchPlayer();
                    System.out.println(player.getCurrentName() + " Win!!");
                    draw = false;
                    break;
                }
            }
            
        }
        if(draw == true)
            System.out.println("Draw");
        
    }
}
