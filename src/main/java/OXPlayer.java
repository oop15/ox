/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class OXPlayer {
    
    private player a;
    private player b;
    private player current;
    
    public OXPlayer(player a, player b) {
        this.a = a;
        this.b = b;
        this.current = this.randomPlayer();
    }
    public void switchPlayer() {
        if(current == a)
            this.current = b;
        else
            this.current = a;
        
    }
    
    public char getCurrentName() {
        return this.current.getName();
    }
    
    public player getCurrentPlayer() {
        return this.current;
    }
    
    private player randomPlayer() {
        int rand = (int) Math.round(Math.random() * 1);
        if(rand == 0)
            return a;
        return b;
    }
}
