/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class table {

    private char[][] arr;
    private player play;
    private int row;
    private int col;

    public table(int row, int col) {
        this.row = row;
        this.col = col;
        this.arr = new char[row][col];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                this.arr[i][j] = '-';

            }
        }
    }
    
    public int startRandom() {
        int rand = (int) Math.round(Math.random() * 1) -1 ;
        return rand;
    }
    public void Maptable() {
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.print(this.arr[i][j] + " ");

            } 
            System.out.println("");
        }
    }

    public void setplay(int row, int col, player play) {
        this.arr[row][col] = play.getName();

    }

    public boolean check(int row, int col) {
        if(row < 0 || row > 2 || col < 0 || col > 2){
            return false;
        }
            
        if (this.arr[row][col] == '-') {
            return true;
        } else {
            return false;
        }

    }

    public boolean checkwin() {
        if(checkRow() || checkCol() || checkCross()) {
            return true;
        }
        return false;               
    }
    
    private boolean checkRow() {
        String tmp = "";
        for (int i = 0; i < this.row; i++) {
            for (int j = 0; j < this.col; j++) {
                tmp += this.arr[i][j];
            }
            if(tmp.equalsIgnoreCase("ooo") || tmp.equalsIgnoreCase("xxx")) {
                return true;
            }
            tmp = "";
        }
        
        return false;
    }
    private boolean checkCol() {
        String tmp = "";
        for (int i = 0; i < this.row; i++) {
            for (int j = 0; j < this.col; j++) {
                tmp += this.arr[j][i];
            } 
            if(tmp.equalsIgnoreCase("ooo") || tmp.equalsIgnoreCase("xxx")) {
                return true;
            }
            tmp = "";
        }
        
        return false;
    }
    private boolean checkCross() {
        String tmp = this.arr[0][0]+""+this.arr[1][1]+""+this.arr[2][2];
        String tmp2 = this.arr[0][2]+""+this.arr[1][1]+""+this.arr[2][0];
        if(tmp.equalsIgnoreCase("ooo") || tmp.equalsIgnoreCase("xxx") || tmp2.equalsIgnoreCase("ooo") || tmp2.equalsIgnoreCase("xxx")) {
                return true;
            }
        return false;
    }
}
